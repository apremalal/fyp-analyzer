#ifndef DEEPZOOMCOMPOSER_H
#define DEEPZOOMCOMPOSER_H

#include <QtCore/QThread>
#include <QString>

#include "Config.h"

#import "dependencies\ZoomComposer.tlb"

using namespace ZoomComposer;


class DeepZoomComposer: public QThread
{
    Q_OBJECT
public:
    DeepZoomComposer(int compositionType, QString source, QString destination, int tileSize, double imageQuality, int tileOverlap,bool createCollection);
    bool composeSingleImage(QString source, QString destination, int tileSize, double imageQuality, int tileOverlap);
    bool composeMultipleImages(QString source, QString destination, bool createCollection, int tileSize, double imageQuality, int tileOverlap);

private:
    IDeepZoomComposerPtr pZoomComposer;
    QString source;
    QString destination;
    int tileSize;
    double imageQuality;
    int tileOverlap;
    bool createCollection;
    int compositionType;

protected:
    void run();

signals:
    void compositionComplete();
};

#endif // DEEPZOOMCOMPOSER_H
