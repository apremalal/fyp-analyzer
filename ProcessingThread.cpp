#include "ProcessingThread.h"

ProcessingThread::ProcessingThread(SharedImageBuffer *sharedImageBuffer, int deviceNumber, SharedSettings *sharedSettings, QSerialPort *serialPort) : QThread(), sharedImageBuffer(sharedImageBuffer), sharedSettings(sharedSettings),serialPort(serialPort)
{
    this->deviceNumber=deviceNumber;
    doStop=false;
    sampleNumber=0;
    fpsSum=0; capturedImageCount = 0; wSICaptured = false;
    sharedSettings->focusThreshold = DEFAULT_FOCUS_THRESHOLD;
    fps.clear();
    statsData.averageFPS=0;
    statsData.nFramesProcessed=0;
    WSIPropertiesFile = NULL;
    initNewImageStore();
}

void ProcessingThread::run()
{
    while(1)
    {
        doStopMutex.lock();
        if(doStop)
        {
            doStop=false;
            doStopMutex.unlock();
            break;
        }
        doStopMutex.unlock();
        processingTime=t.elapsed();// Save processing time
        t.start();      // Start timer (used to calculate processing rate)

        processingMutex.lock();
        // Get frame from queue, store in currentFrame, set ROI
        currentFrame = Mat(sharedImageBuffer->getByDeviceNumber(deviceNumber)->get().clone());

        if(serialCommunicationFlags.scanningOnProgress){
            switch (serialCommunicationFlags.operationStatus) {
            case SUCCESS:
                switch (serialCommunicationFlags.currentOperation) {
                case READY_TO_CAPTURE:
                    imwrite((currentImageStoreLoc+QString::number(capturedImageCount)+".jpg").toLocal8Bit().constData(),currentFrame);
                    capturedImageCount++;
                    transmitPacket.mode = AUTOMATIC_MODE;
                    transmitPacket.command = MOVING_TO_NEXT_TILE;
                    serialCommunicationFlags.operationStatus = PENDING;
                    if(isNextTileParamsExists()){
                        serialCommunicationFlags.operationStatus = PENDING;
                        sendCommand();
                    }else{
                        emit scanCompleteSignal(currentImageStoreLoc);
                        serialCommunicationFlags.scanningOnProgress = false;
                        wSICaptured = true;
                        transmitPacket.command = MOVE_TO_INITIAL_POSITION;
                        transmitPacket.opt1 = 2;//sharedSettings->scanningBlock;
                        transmitPacket.opt2 = -1;
                        transmitPacket.opt3 = -1;
                        sendCommand();
                    }
                    break;
                case MOVE_TO_INITIAL_POSITION:
                    transmitPacket.mode = AUTOMATIC_MODE;
                    transmitPacket.command = MOVE_TO_INITIAL_POSITION;
                    transmitPacket.opt1 =2;// sharedSettings->scanningBlock;
                    transmitPacket.opt2 = -1;
                    transmitPacket.opt3 = -1;
                    sendCommand();
                    serialCommunicationFlags.operationStatus = PENDING;
                    break;
                case FOCUS_COMPLETE:
                    break;
                case SCAN_COMPLETE:
                    break;
                default:
                    break;
                }
                break;
            case PENDING:
                break;
            case FAILED:
                qDebug() << "Something went wrong";
                break;
            }
        }

        if(currentFrame.data)
            frameData.focusMeasure = FocusMeasure(currentFrame);

        processingMutex.unlock();
        emit newFrameData(frameData);
        frame = MatToQImage(currentFrame);

        emit newFrame(frame);// Inform GUI thread of new frame (QImage)

        updateFPS(processingTime);// Update statistics
        statsData.nFramesProcessed++;
        emit updateStatisticsInGUI(statsData);        // Inform GUI of updated statistics
    }
    qDebug() << "Stopping processing thread...";
}

bool ProcessingThread::isNextTileParamsExists(){

    switch (sharedSettings->currentXDirection) {
    case DIRECTION_PLUS:
        if(sharedSettings->currentX+1<=sharedSettings->nXTiles)
        {
            sharedSettings->currentX++;
            transmitPacket.opt1 = MOTOR_X;
            transmitPacket.opt2 = DIRECTION_PLUS;
            transmitPacket.opt3 = sharedSettings->calculatedStepSizeX;
        }else if(sharedSettings->currentY+1<=sharedSettings->nYTiles)
        {
            sharedSettings->currentY++;
            sharedSettings->currentXDirection = DIRECTION_MINUS;
            transmitPacket.opt1 = MOTOR_Y;
            transmitPacket.opt2 = DIRECTION_PLUS;
            transmitPacket.opt3 = sharedSettings->calculatedStepSizeY;
        }else
            return false;
        break;
    case DIRECTION_MINUS:
        if(sharedSettings->currentX-1 >= 0)
        {
            sharedSettings->currentX--;
            transmitPacket.opt1 = MOTOR_X;
            transmitPacket.opt2 = DIRECTION_MINUS;
            transmitPacket.opt3 = sharedSettings->calculatedStepSizeX;
        }else if(sharedSettings->currentY+1<=sharedSettings->nYTiles)
        {
            sharedSettings->currentY++;
            sharedSettings->currentXDirection = DIRECTION_PLUS;
            transmitPacket.opt1 = MOTOR_Y;
            transmitPacket.opt2 = DIRECTION_MINUS;
            transmitPacket.opt3 = sharedSettings->calculatedStepSizeY;
        }else
            return false;
        break;
    }
    return true;
}

QByteArray ProcessingThread::readData(){
    receivedPacket = serialPort->readAll();
    if (!receivedPacket.isEmpty()){
        for(int i=0;i<receivedPacket.length();i++)
        {
            if(receivedPacket.at(i) != '\n'){
                buffer.append(receivedPacket.at(i));
            }else{
                QString stringifiedData(buffer.mid(0,buffer.length()-1));
                qDebug() << "received packet : "+ stringifiedData;
                QStringList data = stringifiedData.split(":");
                if(data.size()== 3)
                {
                    responcePacket.sequenceNo = data.at(0).toInt();
                    responcePacket.operationStatus = data.at(1).toInt();
                    responcePacket.command = data.at(2).toInt();
                }
                if(responcePacket.operationStatus == SUCCESS){
                    serialCommunicationFlags.operationStatus = SUCCESS;
                    switch (responcePacket.command) {
                    case MOVE_TO_INITIAL_POSITION: // initializatoin complete
                        serialCommunicationFlags.currentOperation = READY_TO_CAPTURE;
                        break;

                    case MOVING_TO_NEXT_TILE: // moved to tile
                        serialCommunicationFlags.currentOperation = READY_TO_CAPTURE;
                        break;

                    case DO_FOCUS: // focus complete
                        break;

                    default:
                        break;
                    }
                }else
                    qDebug() << "Something went wrong on the HW side";
                buffer.clear();
            }
        }
    }
    return receivedPacket;
}

QByteArray ProcessingThread::getEncodedTransmitPacket()
{
    QString stringPacket = QString::number(transmitPacket.sequenceNo)+","+ QString::number(transmitPacket.mode)+","+ QString::number(transmitPacket.command)+","+ QString::number(transmitPacket.opt1)+","+ QString::number(transmitPacket.opt2)+","+ QString::number(transmitPacket.opt3);
    QByteArray encodedTransmitPacket(stringPacket.toStdString().c_str());
    return encodedTransmitPacket;
}

QString ProcessingThread::initNewImageStore(){
    QDateTime local(QDateTime::currentDateTime());
    timeStamp = QString::number(local.toTime_t());
    currentImageStoreLoc = sharedSettings->wSIImageStoreLoc +"/"+ timeStamp+"/";
    if (!QDir(currentImageStoreLoc).exists()){
        QDir().mkpath(currentImageStoreLoc);
    }
    return currentImageStoreLoc;
}

bool ProcessingThread::writeWSIPropertiesToFile()
{
    if(WSIPropertiesFile)
        delete WSIPropertiesFile;
    WSIPropertiesFile = new QFile(currentImageStoreLoc+"/.awsi");

    QTextStream out(WSIPropertiesFile);
    QString jsonProperties = "{\n\"zoomLevel\":"+QString::number(sharedSettings->zoomLevel)+",\n\"frameSize\":\""+sharedSettings->frameSize+"\",\n"+"\"timeStamp:"+timeStamp+",\n\"capturedImageCount\":"+QString::number(capturedImageCount)+",\n\"captureSuccess\":"+wSICaptured+"}";
    out << jsonProperties;
    out.flush();
    return true;
}

void ProcessingThread::updateImageProcessingFlags(struct ImageProcessingFlags imgProcFlags)
{
    QMutexLocker locker(&processingMutex);
    this->imgProcFlags.grayscaleOn=imgProcFlags.grayscaleOn;
    this->imgProcFlags.smoothOn=imgProcFlags.smoothOn;
    this->imgProcFlags.dilateOn=imgProcFlags.dilateOn;
    this->imgProcFlags.erodeOn=imgProcFlags.erodeOn;
    this->imgProcFlags.flipOn=imgProcFlags.flipOn;
    this->imgProcFlags.cannyOn=imgProcFlags.cannyOn;
}

void ProcessingThread::updateImageProcessingSettings(struct ImageProcessingSettings imgProcSettings)
{
    QMutexLocker locker(&processingMutex);
    this->imgProcSettings.smoothType=imgProcSettings.smoothType;
    this->imgProcSettings.smoothParam1=imgProcSettings.smoothParam1;
    this->imgProcSettings.smoothParam2=imgProcSettings.smoothParam2;
    this->imgProcSettings.smoothParam3=imgProcSettings.smoothParam3;
    this->imgProcSettings.smoothParam4=imgProcSettings.smoothParam4;
    this->imgProcSettings.dilateNumberOfIterations=imgProcSettings.dilateNumberOfIterations;
    this->imgProcSettings.erodeNumberOfIterations=imgProcSettings.erodeNumberOfIterations;
    this->imgProcSettings.flipCode=imgProcSettings.flipCode;
    this->imgProcSettings.cannyThreshold1=imgProcSettings.cannyThreshold1;
    this->imgProcSettings.cannyThreshold2=imgProcSettings.cannyThreshold2;
    this->imgProcSettings.cannyApertureSize=imgProcSettings.cannyApertureSize;
    this->imgProcSettings.cannyL2gradient=imgProcSettings.cannyL2gradient;
}

void ProcessingThread::handleError(QSerialPort::SerialPortError error)
{
    if (error == QSerialPort::ResourceError) {
        qDebug() << serialPort->errorString();
    }
}

void ProcessingThread::setROI(QRect roi)
{
    QMutexLocker locker(&processingMutex);
    currentROI.x = roi.x();
    currentROI.y = roi.y();
    currentROI.width = roi.width();
    currentROI.height = roi.height();
}

void ProcessingThread::stop()
{
    QMutexLocker locker(&doStopMutex);
    doStop=true;
}

void ProcessingThread::updateSerialCommunicatoinFlags(struct SerialCommunicationFlags serialComFlags)
{
    QMutexLocker locker(&processingMutex);
    this->serialCommunicationFlags.camMovingToNextTile = serialComFlags.camMovingToNextTile;
    this->serialCommunicationFlags.camOnMove = serialComFlags.camOnMove;
    this->serialCommunicationFlags.doFocus = serialComFlags.doFocus;
    this->serialCommunicationFlags.focusComplete = serialComFlags.focusComplete;
    this->serialCommunicationFlags.movingToInitial = serialComFlags.movingToInitial;
    this->serialCommunicationFlags.readyToCapture = serialComFlags.readyToCapture;
    this->serialCommunicationFlags.reset = serialComFlags.reset;
    this->serialCommunicationFlags.resetComplete = serialComFlags.resetComplete;
    this->serialCommunicationFlags.scanComplete = serialComFlags.scanComplete;
    this->serialCommunicationFlags.stopScanning = serialComFlags.stopScanning;
    this->serialCommunicationFlags.tileStable = serialComFlags.tileStable;
}

QRect ProcessingThread::getCurrentROI()
{
    return QRect(currentROI.x, currentROI.y, currentROI.width, currentROI.height);
}

void ProcessingThread::updateFPS(int timeElapsed)
{
    if(timeElapsed>0)
    {
        fps.enqueue((int)1000/timeElapsed);
        sampleNumber++;
    }

    // Maximum size of queue is DEFAULT_PROCESSING_FPS_STAT_QUEUE_LENGTH
    if(fps.size()>PROCESSING_FPS_STAT_QUEUE_LENGTH)
        fps.dequeue();

    // Update FPS value every DEFAULT_PROCESSING_FPS_STAT_QUEUE_LENGTH samples
    if((fps.size()==PROCESSING_FPS_STAT_QUEUE_LENGTH)&&(sampleNumber==PROCESSING_FPS_STAT_QUEUE_LENGTH))
    {
        // Empty queue and store sum
        while(!fps.empty())
            fpsSum+=fps.dequeue();
        // Calculate average FPS
        statsData.averageFPS=fpsSum/PROCESSING_FPS_STAT_QUEUE_LENGTH;
        // Reset sum
        fpsSum=0;
        // Reset sample number
        sampleNumber=0;
    }
}

void ProcessingThread::sendCommand()
{
    writeData(getEncodedTransmitPacket());
}

void ProcessingThread::writeData(const QByteArray &data){
    if(serialPort->isOpen())
    {
        serialPort->write(data);
        serialPort->flush();
        qDebug() << "transmitedpacket :" + QString(data);
    }
}
