#ifndef FOCUSTHRESHOLD_H
#define FOCUSTHRESHOLD_H

#include <opencv/cv.h>
#include <opencv/highgui.h>

using namespace cv;

double FocusMeasure(Mat image);

#endif // FOCUSTHRESHOLD_H
