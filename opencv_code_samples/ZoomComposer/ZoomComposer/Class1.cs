﻿
using Microsoft.DeepZoomTools;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZoomComposer
{
    public interface IDeepZoomComposer
    {
        bool composeSingleImage(string sourceImage, string destination, int tileSize, double imageQuality, int tileOverlap);
        bool composeMultipleImages(string source, string destination, bool createCollection, int tileSize, double imageQuality, int tileOverlap);
    };

    public class Composer : IDeepZoomComposer
    {
        public bool composeSingleImage(string sourceImage, string destination, int tileSize, double imageQuality, int tileOverlap)
        {
            ImageCreator ic = new ImageCreator();

            ic.TileSize = tileSize;
            ic.TileFormat = ImageFormat.Jpg;
            ic.ImageQuality = imageQuality;
            ic.TileOverlap = tileOverlap;
            string target = destination + "\\output_images\\" + Path.GetFileNameWithoutExtension(sourceImage);
            ic.Create(sourceImage, target);
            return true;
        }

        public bool composeMultipleImages(string source, string destination, bool createCollection, int tileSize, double imageQuality, int tileOverlap)
        {
            List<string> images = GetImagesInDirectory(source);
            List<string> data = new List<string>();

            foreach (var image in images)
            {
                ImageCreator ic = new ImageCreator();

                ic.TileSize = tileSize;
                ic.TileFormat = ImageFormat.Jpg;
                ic.ImageQuality = imageQuality;
                ic.TileOverlap = tileOverlap;

                string target = destination + "\\output_images\\" + Path.GetFileNameWithoutExtension(image);

                ic.Create(image, target);
                data.Add(Path.ChangeExtension(target, ".xml"));
            }

            if (createCollection)
            {
                CollectionCreator cc = new CollectionCreator();
                cc.TileSize = 256;
                cc.TileFormat = ImageFormat.Jpg;
                cc.MaxLevel = 8;
                cc.ImageQuality = 0.95;
                cc.Create(data, destination + "\\output");
            }
            return true;
        }

        private static List<string> GetImagesInDirectory(string path)
        {
            return GetImagesInDirectory(new DirectoryInfo(path));
        }

        private static List<string> GetImagesInDirectory(DirectoryInfo di)
        {
            List<string> images = new List<string>();

            foreach (var fi in di.GetFiles("*.jpg"))
            {
                images.Add(fi.FullName);
            }

            foreach (var sub in di.GetDirectories())
            {
                images.AddRange(GetImagesInDirectory(sub));
            }
            return images;
        }
    }
}
