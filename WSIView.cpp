#include "WSIView.h"
#include "ui_WSIView.h"

WSIView::WSIView(QWidget *parent, SharedSettings *sharedSettings) :
    QWidget(parent),
    ui(new Ui::WSIView),sharedSettings(sharedSettings)
{
    ui->setupUi(this);
    ui->comboBoxWSIMethod->addItem(QLatin1String("Stitching"), WSI_STITCHING);
    ui->comboBoxWSIMethod->addItem(QLatin1String("Tiling"), WSI_TILING);
    ui->webViewWSI->settings()->setAttribute(QWebSettings::LocalContentCanAccessFileUrls,true);

#ifdef APPLICATION_MODE == "dev"
    ui->webViewWSI->load(QUrl::fromLocalFile("C:/Users/Anuruddha/git/FYP/fyp-analyzer/plugins/seadragon/index.html"));
#else
    ui->webViewWSI->load(QUrl::fromLocalFile(QDir::currentPath()+"plugins/seadragon/index.html"));
#endif

    frame = ui->webViewWSI->page()->mainFrame();
    movie = new QMovie(":/new/controller/Resources/progressBar.gif");
    ui->labelProgress->setVisible(false);
    ui->labelProgress->setMovie(movie);
    movie->start();
}

WSIView::~WSIView()
{
    delete ui;
}

void createWSI(int method){
    switch (method) {
    case WSI_TILING:
        break;
    case WSI_STITCHING:
        break;
    default:
        break;
    }
}

void WSIView::on_pushButtonLoadWSI_clicked()
{
    QFileDialog fd;
    QString selfilter = tr("WSI (*.xml)");
    QString fileName = fd.getOpenFileName(this, tr("Select a WSI File"),"", tr("All files (*.*);;WSI (*.xml)" ),&selfilter );
    if (fileName != "")
        frame->evaluateJavaScript("load('"+QUrl::fromLocalFile(fileName).toString()+"')");
}

void WSIView::on_pushButtonImportRawImage_clicked()
{
    QFileDialog fd;
    QString selfilter = tr("JPEG (*.jpg *.jpeg)");
    QString fullFileName = fd.getOpenFileName(this, tr("Select File"),"", tr("All files (*.*);;JPEG (*.jpg *.jpeg);;TIFF (*.tif)" ),&selfilter );
    QFile file(fullFileName);
    QFileInfo fileInfo(file.fileName());
    QString fileName(fileInfo.baseName());
    if (fullFileName != ""){
        showBusy(true);
        importedFileName = QUrl::fromLocalFile(sharedSettings->wSIImageStoreLoc+"\\output_images\\"+fileName+".xml").toString();
        composer = new DeepZoomComposer(SINGLE_IMAGE_COMPOSER,fullFileName,sharedSettings->wSIImageStoreLoc,256,0.95,0,false);
        composer->start((QThread::Priority)5);
        connect(composer, SIGNAL(compositionComplete()), this, SLOT(renderDeepZoomImage()));
    }

}

void WSIView::renderDeepZoomImage()
{
    showBusy(false);
    frame->evaluateJavaScript("load('"+importedFileName+"')");
}

void WSIView::showBusy(bool show)
{
    ui->labelProgress->setVisible(show);
}
