#include "SlideView.h"
#include "ui_SlideView.h"
#include <Windows.h>
#include <QMessageBox>

SlideView::SlideView(QWidget *parent, int deviceNumber, SharedImageBuffer *sharedImageBuffer, SharedSettings *sharedSettings) :
    QWidget(parent),
    ui(new Ui::SlideView),
    sharedImageBuffer(sharedImageBuffer),sharedSettings(sharedSettings)
{
    ui->setupUi(this);

    initController();

    ui->pushButtonCameraConnectDisconnect->setFocus();

    connect(ui->pushButtonUp, SIGNAL(pressed()),this, SLOT(on_navigation_pushButton_pressed()));
    connect(ui->pushButtonDown,SIGNAL(pressed()),this, SLOT(on_navigation_pushButton_pressed()));
    connect(ui->pushButtonLeft, SIGNAL(pressed()),this, SLOT(on_navigation_pushButton_pressed()));
    connect(ui->pushButtonRight,SIGNAL(pressed()),this, SLOT(on_navigation_pushButton_pressed()));
    connect(ui->toolButtonZoomIn,SIGNAL(pressed()),this, SLOT(on_navigation_pushButton_pressed()));
    connect(ui->toolButtonZoomOut,SIGNAL(pressed()),this, SLOT(on_navigation_pushButton_pressed()));

    connect(ui->pushButtonUp, SIGNAL(released()),this, SLOT(on_navigation_pushButton_released()));
    connect(ui->pushButtonDown,SIGNAL(released()),this, SLOT(on_navigation_pushButton_released()));
    connect(ui->pushButtonLeft, SIGNAL(released()),this, SLOT(on_navigation_pushButton_released()));
    connect(ui->pushButtonRight,SIGNAL(released()),this, SLOT(on_navigation_pushButton_released()));
    connect(ui->toolButtonZoomIn,SIGNAL(released()),this, SLOT(on_navigation_pushButton_released()));
    connect(ui->toolButtonZoomOut,SIGNAL(released()),this, SLOT(on_navigation_pushButton_released()));

    connect(ui->pushButtonCameraConnectDisconnect,SIGNAL(clicked()),this, SLOT(connectDisconnectCamera()));
    connect(ui->radioButton100X, SIGNAL(clicked()),this, SLOT(on_zoomLevel_changed()));
    connect(ui->radioButton400X, SIGNAL(clicked()),this, SLOT(on_zoomLevel_changed()));
    connect(ui->radioButton1000X, SIGNAL(clicked()),this, SLOT(on_zoomLevel_changed()));

    serialPort = new QSerialPort(this);

    // Save Device Number
    this->deviceNumber= 0;
    // Initialize internal flag
    isCameraConnected = false;
    doRecord = false;

    // Initialize ImageProcessingFlags structure
    imageProcessingFlags.grayscaleOn=false;
    imageProcessingFlags.smoothOn=false;
    imageProcessingFlags.dilateOn=false;
    imageProcessingFlags.erodeOn=false;
    imageProcessingFlags.flipOn=false;
    imageProcessingFlags.cannyOn=false;

    //initializing Serial communication flags
    serialCommunicationFlags.scanningOnProgress = false;

    // Connect signals/slots
    connect(ui->frameLabel, SIGNAL(onMouseMoveEvent()), this, SLOT(updateMouseCursorPosLabel()));
    connect(ui->frameLabel->menu, SIGNAL(triggered(QAction*)), this, SLOT(handleContextMenuAction(QAction*)));
    // Register type
    qRegisterMetaType<struct ThreadStatisticsData>("ThreadStatisticsData");
    qRegisterMetaType<struct FrameData>("FrameData");
    qRegisterMetaType<QString>("QString");
    connectToDevice();
}

SlideView::~SlideView()
{
    if(isCameraConnected)
    {
        // Stop processing thread
        if(processingThread->isRunning())
            stopProcessingThread();
        // Stop capture thread
        if(captureThread->isRunning())
            stopCaptureThread();

        // Automatically start frame processing (for other streams)
        if(sharedImageBuffer->isSyncEnabledForDeviceNumber(deviceNumber))
            sharedImageBuffer->setSyncEnabled(true);

        // Remove from shared buffer
        sharedImageBuffer->removeByDeviceNumber(deviceNumber);
        // Disconnect camera
        if(captureThread->disconnectCamera())
            qDebug() << "[" << deviceNumber << "] Camera successfully disconnected.";
        else
            qDebug() << "[" << deviceNumber << "] WARNING: Camera already disconnected.";
    }
    closeDeviceConnection();
    // Delete UI
    delete ui;
}

void SlideView::initController()
{
    ui->lineEditFocusThreshold->setText(QString::number(DEFAULT_FOCUS_THRESHOLD));
    ui->radioButton100X->setChecked(true);
    //slide size
    ui->slideSizeBox->addItem(QLatin1String("12x2"),3);
    ui->spinBoxWSIOverlapSize->setMaximum(MAXIMUM_FRAME_OVERLAP_SIZE);
    ui->spinBoxWSIOverlapSize->setMinimum(MINIMUM_FRAME_OVERLAP_SIZE);
    ui->spinBoxWSIOverlapSize->setValue(DEFAULT_FRAME_OVERLAP_SIZE);

    //camera view sizes
    ui->cameraViewSizeBox->addItem(QLatin1String("640x480"),"640");

    //scannig block
    ui->scanningBlockBox->addItem(QLatin1String("1"),1);
    ui->scanningBlockBox->addItem(QLatin1String("2"),2);
    ui->scanningBlockBox->addItem(QLatin1String("3"),3);

    if(isDeviceConnected())
    {
        ui->pushButtonCameraConnectDisconnect->setEnabled(true);//enable controller functionality
        ui->pushButtonDeviceConnectDisconnect->setEnabled(true);
        ui->pushButtonCaptureTile->setEnabled(true);
        ui->pushButtonReset->setEnabled(true);
        ui->pushButtonStartStopScanning->setEnabled(true);
        ui->pushButtonUpdateFocus->setEnabled(true);
        ui->pushButtonRecord->setEnabled(true);
    }
    else
    {
        ui->pushButtonCameraConnectDisconnect->setEnabled(false);//disable controller
        ui->pushButtonDeviceConnectDisconnect->setEnabled(false);
        ui->pushButtonCaptureTile->setEnabled(false);
        ui->pushButtonReset->setEnabled(false);
        ui->pushButtonStartStopScanning->setEnabled(false);
        ui->pushButtonUpdateFocus->setEnabled(false);
        ui->pushButtonRecord->setEnabled(false);
    }
    sharedSettings->zoomLevel = 400;
    sharedSettings->frameSize = "640:480";
}

bool SlideView::connectToCamera(bool dropFrameIfBufferFull, int capThreadPrio, int procThreadPrio, bool enableFrameProcessing, int width, int height)
{
    // Set frame label text
    if(sharedImageBuffer->isSyncEnabledForDeviceNumber(deviceNumber))
        ui->frameLabel->setText("Camera connected. Waiting...");
    else
        ui->frameLabel->setText("Connecting to camera...");
    Buffer<Mat> *imageBuffer = new Buffer<Mat>(DEFAULT_IMAGE_BUFFER_SIZE);
    sharedImageBuffer->add(0, imageBuffer);
    // Create capture thread
    captureThread = new CaptureThread(sharedImageBuffer, sharedSettings, deviceNumber, dropFrameIfBufferFull, width, height);
    // Attempt to connect to camera
    if(captureThread->connectToCamera())
    {
        processingThread = new ProcessingThread(sharedImageBuffer, deviceNumber, sharedSettings, serialPort);
        connect(serialPort, SIGNAL(readyRead()), processingThread, SLOT(readData()));
        connect(serialPort, SIGNAL(error(QSerialPort::SerialPortError)), processingThread,SLOT(handleError(QSerialPort::SerialPortError)));

        connect(this, SIGNAL(newSerialCommunicationFlags(struct SerialCommunicationFlags)), processingThread, SLOT(updateSerialCommunicatoinFlags(struct SerialCommunicationFlags)));
        connect(processingThread, SIGNAL(newFrame(QImage)), this, SLOT(updateFrame(QImage))); //show image
        connect(processingThread, SIGNAL(updateStatisticsInGUI(struct ThreadStatisticsData)), this, SLOT(updateProcessingThreadStats(struct ThreadStatisticsData)));
        connect(processingThread, SIGNAL(newFrameData(struct FrameData)), this, SLOT(updateFrameData(struct FrameData)));
        connect(processingThread, SIGNAL(scanCompleteSignal(QString)), this, SLOT(finalizeScan(QString)));

        connect(captureThread, SIGNAL(updateStatisticsInGUI(struct ThreadStatisticsData)), this, SLOT(updateCaptureThreadStats(struct ThreadStatisticsData)));

        connect(this, SIGNAL(newImageProcessingFlags(struct ImageProcessingFlags)), processingThread, SLOT(updateImageProcessingFlags(struct ImageProcessingFlags)));
        connect(this, SIGNAL(setROI(QRect)), processingThread, SLOT(setROI(QRect)));

        // Only enable ROI setting/resetting if frame processing is enabled
        if(enableFrameProcessing)
            connect(ui->frameLabel, SIGNAL(newMouseData(struct MouseData)), this, SLOT(newMouseData(struct MouseData)));
        // Set initial data in processing thread
        emit setROI(QRect(0, 0, captureThread->getInputSourceWidth(), captureThread->getInputSourceHeight()));
        emit newImageProcessingFlags(imageProcessingFlags);

        // Start capturing frames from camera
        captureThread->start((QThread::Priority)capThreadPrio);
        // Start processing captured frames (if enabled)
        if(enableFrameProcessing)
            processingThread->start((QThread::Priority)procThreadPrio);

        // Set internal flag and return
        isCameraConnected=true;
        // Set frame label text
        if(!enableFrameProcessing)
            ui->frameLabel->setText("Frame processing disabled.");
        return true;

    }
    else
        return false;
}

void SlideView::on_pushButtonStartStopScanning_clicked()
{
    if(hardwareProperties.isPortOpened)
    {
        if(serialCommunicationFlags.scanningOnProgress)
        {
            setManualControlEnabled(true);            
            ui->pushButtonStartStopScanning->setIcon(QIcon(QPixmap(":/new/controller/Resources/play-512.png")));
            ui->pushButtonCameraConnectDisconnect->setEnabled(true);
            serialCommunicationFlags.scanningOnProgress = false;
            serialCommunicationFlags.operationStatus = SUCCESS;
            serialCommunicationFlags.currentOperation = SCAN_TERMINATED_MANUALLY;
            emit newSerialCommunicationFlags(serialCommunicationFlags);
            processingThread->writeWSIPropertiesToFile();
        }else{
            if(!isCameraConnected)
            {
               connectDisconnectCamera();
            }
            processingThread->initNewImageStore();
            sharedSettings->fullFrameStepSizeX = ABS_TILE_WIDTH;
            sharedSettings->fullFrameStepSizeY = ABS_TILE_HEIGHT;

            sharedSettings->overlapSize = 20;

            sharedSettings->nXTiles = sharedSettings->zoomLevel*(ABS_BLOCK_WIDTH)/sharedSettings->fullFrameStepSizeX;
            sharedSettings->nYTiles = sharedSettings->zoomLevel*(ABS_BLOCK_HEIGHT)/sharedSettings->fullFrameStepSizeY;

            sharedSettings->calculatedStepSizeX = sharedSettings->fullFrameStepSizeX*(1-(sharedSettings->overlapSize/100));
            sharedSettings->calculatedStepSizeY = sharedSettings->fullFrameStepSizeY*(1-(sharedSettings->overlapSize/100));

            qDebug() << "Xsteps"+ QString::number(sharedSettings->calculatedStepSizeX);
            qDebug() << "Ysteps"+ QString::number(sharedSettings->calculatedStepSizeY);

            sharedSettings->currentX = 0;
            sharedSettings->currentY = 0;
            sharedSettings->currentXDirection = DIRECTION_PLUS;
            sharedSettings->currentYDirection = DIRECTION_PLUS;

            //setManualControlEnabled(false);
            ui->pushButtonCameraConnectDisconnect->setEnabled(false);
            ui->pushButtonStartStopScanning->setIcon(QIcon(QPixmap(":/new/controller/Resources/stop-512.png")));

            processingThread->serialCommunicationFlags.scanningOnProgress = true;
            processingThread->serialCommunicationFlags.operationStatus = SUCCESS;
            processingThread->serialCommunicationFlags.currentOperation = MOVE_TO_INITIAL_POSITION;
        }
    }else
    {
        QMessageBox::warning(this,"ERROR:","Please connect the device before begin the scanning.");
    }
}

void SlideView::connectDisconnectCamera()
{
    if(!isCameraConnected)
    {
        if(connectToCamera(false,4,5,true,1000,1000))
        {
            ui->pushButtonCameraConnectDisconnect->setIcon(QIcon(QPixmap(":/new/controller/Resources/video-off.png")));
        }
    }else
    {
        ui->pushButtonCameraConnectDisconnect->setIcon(QIcon(QPixmap(":/new/controller/Resources/video-on.png")));
        if(processingThread->isRunning())
            stopProcessingThread();
        if(captureThread->isRunning())
            stopCaptureThread();

        if(sharedImageBuffer->isSyncEnabledForDeviceNumber(deviceNumber))
            sharedImageBuffer->setSyncEnabled(true);

        sharedImageBuffer->removeByDeviceNumber(deviceNumber);

        captureThread->disconnectCamera();
        isCameraConnected = false;
    }
}

void SlideView::on_pushButtonRecord_clicked()
{
    if(isCameraConnected)
    {
        if(doRecord){
            ui->pushButtonRecord->setIcon(QIcon(QPixmap(":/new/controller/Resources/startrecording.png")));
            captureThread->stopRecording();
            doRecord = false;
        }
        else{
            ui->pushButtonRecord->setIcon(QIcon(QPixmap(":/new/controller/Resources/stoprecording.png")));
            doRecord = true;
            captureThread->startRecording();
        }
    }
}

void SlideView::on_navigation_pushButton_pressed()
{
    if(!serialCommunicationFlags.scanningOnProgress){
        QPushButton *button = (QPushButton *)sender();
        QString objectName = button->objectName();
        if(objectName.compare("pushButtonUp")==0)
        {
            processingThread->writeData("23");
        }
        else if (objectName.compare("pushButtonDown")==0) {
            processingThread->writeData(QByteArray::number(2));
        }
        else if (objectName.compare("pushButtonLeft")==0) {
            processingThread->writeData(QByteArray::number(3));
        }
        else if (objectName.compare("pushButtonRight")==0) {
            processingThread->writeData(QByteArray::number(4));
        }
    }
}

void SlideView::on_navigation_pushButton_released(){
    if(!serialCommunicationFlags.scanningOnProgress){
        processingThread->writeData(QByteArray::number(0));
    }
}

void SlideView::finalizeScan(QString WSILocation){
    ;
}

bool SlideView::connectToDevice()
{
    if(isDeviceConnected())
    {
        if(!hardwareProperties.isPortOpened)
        {
            return openSerialPort();
        }
    }
    else{
        QMessageBox::warning(this,"ERROR:","Please check the device connection!");
        return false;
    }
}

bool SlideView::closeDeviceConnection()
{
    if(isDeviceConnected())
    {
        if(hardwareProperties.isPortOpened)
        {
            serialPort->close();
            hardwareProperties.isPortOpened = false;
            ui->pushButtonDeviceConnectDisconnect->setIcon(QIcon(QPixmap(":/new/controller/Resources/connect.png")));
            return true;
        }
    }
    else{
        QMessageBox::warning(this,"ERROR:","Device disconnected");
        return false;
    }
}

bool SlideView::isDeviceConnected(){
    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts()) {
        QString description = info.description();

        if(description.contains("Arduino")){
            serialPortSettings.name = info.portName();
            hardwareProperties.isDeviceConnected = true;
            break;
        }
        else
            continue;
    }
    return hardwareProperties.isDeviceConnected;
}

bool SlideView::openSerialPort(){
    if(!serialPort->isOpen()){
        serialPort->setPortName(serialPortSettings.name);
        if (serialPort->open(QIODevice::ReadWrite)) {
            if (serialPort->setBaudRate(sharedSettings->serialPortSettings.baudRate)
                    && serialPort->setDataBits(sharedSettings->serialPortSettings.dataBits)
                    && serialPort->setParity(sharedSettings->serialPortSettings.parity)
                    && serialPort->setStopBits(sharedSettings->serialPortSettings.stopBits)
                    && serialPort->setFlowControl(sharedSettings->serialPortSettings.flowControl)) {

                hardwareProperties.isPortOpened = true;
                        ui->pushButtonDeviceConnectDisconnect->setIcon(QIcon(QPixmap(":/new/controller/Resources/disconnect.png")));
                return true;
            } else {
                serialPort->close();
            }
        }
    }
    return false;
}

QByteArray SlideView::readData(){
    QByteArray receivedPacket = serialPort->readAll();
    if (!receivedPacket.isEmpty()){
        for(int i=0;i<receivedPacket.length();i++)
        {
            if(receivedPacket.at(i) != '\n'){
                buffer.append(receivedPacket.at(i));
            }else{
                qDebug() << QString(buffer.mid(0,buffer.length()-1));
                buffer.clear();
            }
        }
    }
    return receivedPacket;
}


void SlideView::updateFrameData(struct FrameData frameData)
{
    ui->labelCurrentFocusMeasure->setText(QString::number(frameData.focusMeasure));
}

void SlideView::setManualControlEnabled(bool enable)
{
    if(enable)
    {
        ui->pushButtonDown->setEnabled(true);
        ui->pushButtonLeft->setEnabled(true);
        ui->pushButtonRight->setEnabled(true);
        ui->pushButtonUp->setEnabled(true);
        ui->toolButtonZoomIn->setEnabled(true);
        ui->toolButtonZoomOut->setEnabled(true);
    }else{
        ui->pushButtonDown->setEnabled(false);
        ui->pushButtonLeft->setEnabled(false);
        ui->pushButtonRight->setEnabled(false);
        ui->pushButtonUp->setEnabled(false);
        ui->toolButtonZoomIn->setEnabled(false);
        ui->toolButtonZoomOut->setEnabled(false);
    }
}

void SlideView::stopCaptureThread()
{
    qDebug() << "[" << deviceNumber << "] About to stop capture thread...";
    captureThread->stop();
    sharedImageBuffer->wakeAll(); // This allows the thread to be stopped if it is in a wait-state
    // Take one frame off a FULL queue to allow the capture thread to finish
    if(sharedImageBuffer->getByDeviceNumber(deviceNumber)->isFull())
        sharedImageBuffer->getByDeviceNumber(deviceNumber)->get();
    captureThread->wait();
    qDebug() << "[" << deviceNumber << "] Capture thread successfully stopped.";
}

void SlideView::stopProcessingThread()
{
    qDebug() << "[" << deviceNumber << "] About to stop processing thread...";
    processingThread->stop();
    sharedImageBuffer->wakeAll(); // This allows the thread to be stopped if it is in a wait-state
    processingThread->wait();

    qDebug() << "[" << deviceNumber << "] Processing thread successfully stopped.";
}

// Display frame
void SlideView::updateFrame(const QImage &frame)
{
    if(isCameraConnected)
        ui->frameLabel->setPixmap(QPixmap::fromImage(frame));
    else
        ui->frameLabel->setText("Camera disconnected ...");
}


void SlideView::on_zoomLevel_changed()
{
    if(ui->radioButton100X->isChecked())
        sharedSettings->zoomLevel = 100;
    else if (ui->radioButton400X->isChecked()) {
        sharedSettings->zoomLevel = 400;
    }
    else
        sharedSettings->zoomLevel = 1000;
}

void SlideView::clearImageBuffer()
{
    if(sharedImageBuffer->getByDeviceNumber(deviceNumber)->clear())
        qDebug() << "[" << deviceNumber << "] Image buffer successfully cleared.";
    else
        qDebug() << "[" << deviceNumber << "] WARNING: Could not clear image buffer.";
}

void SlideView::setImageProcessingSettings()
{}

void SlideView::updateMouseCursorPosLabel()
{}

void SlideView::updateCaptureThreadStats(struct ThreadStatisticsData statData)
{}

void SlideView::updateProcessingThreadStats(struct ThreadStatisticsData statData)
{}

void SlideView::newMouseData(struct MouseData mouseData)
{
    // Local variable(s)
    int x_temp, y_temp, width_temp, height_temp;
    QRect selectionBox;

    // Set ROI
    if(mouseData.leftButtonRelease)
    {
        // Copy box dimensions from mouseData to taskData
        selectionBox.setX(mouseData.selectionBox.x()-((ui->frameLabel->width()-captureThread->getInputSourceWidth()))/2);
        selectionBox.setY(mouseData.selectionBox.y()-((ui->frameLabel->height()-captureThread->getInputSourceHeight()))/2);
        selectionBox.setWidth(mouseData.selectionBox.width());
        selectionBox.setHeight(mouseData.selectionBox.height());
        // Check if selection box has NON-ZERO dimensions
        if((selectionBox.width()!=0)&&((selectionBox.height())!=0))
        {
            // Selection box can also be drawn from bottom-right to top-left corner
            if(selectionBox.width()<0)
            {
                x_temp=selectionBox.x();
                width_temp=selectionBox.width();
                selectionBox.setX(x_temp+selectionBox.width());
                selectionBox.setWidth(width_temp*-1);
            }
            if(selectionBox.height()<0)
            {
                y_temp=selectionBox.y();
                height_temp=selectionBox.height();
                selectionBox.setY(y_temp+selectionBox.height());
                selectionBox.setHeight(height_temp*-1);
            }

            // Check if selection box is not outside window
            if((selectionBox.x()<0)||(selectionBox.y()<0)||
                    ((selectionBox.x()+selectionBox.width())>captureThread->getInputSourceWidth())||
                    ((selectionBox.y()+selectionBox.height())>captureThread->getInputSourceHeight()))
            {
                // Display error message
                QMessageBox::warning(this,"ERROR:","Selection box outside range. Please try again.");
            }
            // Set ROI
            else
                emit setROI(selectionBox);
        }
    }
}

void SlideView::handleContextMenuAction(QAction *action)
{
    if(action->text()=="Reset ROI")
        emit setROI(QRect(0, 0, captureThread->getInputSourceWidth(), captureThread->getInputSourceHeight()));
    else if(action->text()=="Scale to Fit Frame")
        ui->frameLabel->setScaledContents(action->isChecked());
    else if(action->text()=="Grayscale")
    {
        imageProcessingFlags.grayscaleOn=action->isChecked();
        emit newImageProcessingFlags(imageProcessingFlags);
    }
    else if(action->text()=="Smooth")
    {
        imageProcessingFlags.smoothOn=action->isChecked();
        emit newImageProcessingFlags(imageProcessingFlags);
    }
    else if(action->text()=="Dilate")
    {
        imageProcessingFlags.dilateOn=action->isChecked();
        emit newImageProcessingFlags(imageProcessingFlags);
    }
    else if(action->text()=="Erode")
    {
        imageProcessingFlags.erodeOn=action->isChecked();
        emit newImageProcessingFlags(imageProcessingFlags);
    }
    else if(action->text()=="Flip")
    {
        imageProcessingFlags.flipOn=action->isChecked();
        emit newImageProcessingFlags(imageProcessingFlags);
    }
    else if(action->text()=="Canny")
    {
        imageProcessingFlags.cannyOn=action->isChecked();
        emit newImageProcessingFlags(imageProcessingFlags);
    }
    else if(action->text()=="Settings...")
        setImageProcessingSettings();
}

void SlideView::on_cameraViewSizeBox_currentIndexChanged(int index)
{
    switch (index) {
    case 0:
        sharedSettings->frameSize = "640:480";
        break;
    default:
        sharedSettings->frameSize = "640:480";
        break;
    }
}

void SlideView::on_scanningBlockBox_currentIndexChanged(int index)
{
    sharedSettings->scanningBlock = index;
}

void SlideView::on_pushButtonUpdateFocus_clicked()
{
    sharedSettings->focusThreshold = ui->lineEditFocusThreshold->text().toDouble();
}

void SlideView::on_pushButtonCaptureTile_clicked()
{
    captureThread->captureFrame();
}

void SlideView::on_pushButtonDeviceConnectDisconnect_clicked()
{
    if(!hardwareProperties.isPortOpened)
    {
        connectToDevice();
    }else{
        closeDeviceConnection();
    }
}
