#include "wsiframelable.h"
#include <QKeyEvent>
#include <QDebug>

WSIFrameLable::WSIFrameLable(QWidget *parent) :
    QLabel(parent)
{
    currentResolution =100;
    previousResolution = 100;
}


void WSIFrameLable::keyPressEvent(QKeyEvent *ev)
{
    int c = ev->key();
    if( (char)c == '+' ) //zoom in
      {
        currentResolution = (previousResolution==100 || (previousResolution+10) >= 100)?100: (previousResolution+10);
      }
    else if( (char)c == '-' ) // zoom out
     {
       currentResolution = (previousResolution==10 || (previousResolution-10) <= 0)? 10: (previousResolution-10);
     }
    else
        c = 'x';

    if((char)c != 'x' && previousResolution!=currentResolution)
        emit onKeyPressEvent(currentResolution);

    previousResolution = currentResolution;
}
