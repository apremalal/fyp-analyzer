#ifndef WSICREATOR_H
#define WSICREATOR_H

#include <opencv/highgui.h>

#include <QDebug>

using namespace cv;

class WSICreator
{
public:
    WSICreator();
    Mat createStitchedImage();
    Mat createTiledImage(vector<cv::Mat> & images, int cols, int min_gap_size);

};

#endif // WSICREATOR_H
