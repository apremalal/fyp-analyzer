#include "MicrofilariaePlugin.h"

MicrofilariaePlugin::MicrofilariaePlugin(Mat *srcImage, QString imageFileName):QThread(),srcImage(srcImage)
{
    this->imageFileName = imageFileName;
}

void MicrofilariaePlugin::run(){
    if (!(matlabEngine = engOpen(""))) {      
        return;
    }
    engSetVisible(matlabEngine,false);
    engEvalString(matlabEngine, "cd 'Matlab_Component'");
    engEvalString(matlabEngine, ("result = Detector(imread('"+imageFileName+"'));").toLocal8Bit().constData());
    Mat temp ;
    if ((result = engGetVariable(matlabEngine,"result")) == NULL)
     emit matLabResults(temp);
    else {
        temp = srcImage->clone();
        double (*results)[3] = (double (*)[3])mxGetData(result);
        for(int i=0;i<5;i++)
        {
            int radius = (int)results[i][2];
            if (radius==0)
                break;
            int x = (int)results[i][0];
            int y = (int)results[i][1];
            circle(temp,Point(x,y),radius,Scalar(0,0,255));
        }
        emit matLabResults(temp);
    }
}
