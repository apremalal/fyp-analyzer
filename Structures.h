#ifndef STRUCTURES_H
#define STRUCTURES_H

#include <QtCore/QRect>
#include <QtSerialPort/QSerialPort>

struct ImageProcessingSettings{
    int smoothType;
    int smoothParam1;
    int smoothParam2;
    double smoothParam3;
    double smoothParam4;
    double focusThreashold;
    int dilateNumberOfIterations;
    int erodeNumberOfIterations;
    int flipCode;
    double cannyThreshold1;
    double cannyThreshold2;
    int cannyApertureSize;
    bool cannyL2gradient;
};

struct ImageProcessingFlags{
    bool grayscaleOn;
    bool smoothOn;
    bool dilateOn;
    bool erodeOn;
    bool flipOn;
    bool cannyOn;
};

struct MouseData{
    QRect selectionBox;
    bool leftButtonRelease;
    bool rightButtonRelease;
};

struct ThreadStatisticsData{
    int averageFPS;
    int nFramesProcessed;
};

struct FrameData{
    double focusMeasure;
};

struct SerialPortSettings{
    QString name;
    qint32 baudRate;
    QString stringBaudRate;
    QSerialPort::DataBits dataBits;
    QString stringDataBits;
    QSerialPort::Parity parity;
    QString stringParity;
    QSerialPort::StopBits stopBits;
    QString stringStopBits;
    QSerialPort::FlowControl flowControl;
    QString stringFlowControl;
    bool localEchoEnabled;
};

struct SerialCommunicationFlags{
    bool reset; // reset the scanning platform to initial position
    bool movingToInitial;
    bool resetComplete; //resetting complete
    bool tileStable; // camera is in fixed tile position
    bool doFocus; // run focusing
    bool focusComplete; // > ready to capture
    bool readyToCapture;
    bool camOnMove;
    bool camMovingToNextTile;
    bool scanComplete;
    bool stopScanning;
    bool startScanning;
    bool scanningOnProgress;
    int currentOperation;
    int operationStatus;
};

struct WSIMapDetails{
    int X_MAX;
    int Y_MAX;
    int CAMVIEW_HEIGHT;
    int CAMVIEW_WIDTH;
    int SCAN_BLOCK;
    int ZOOM_LEVEL;
    //int MAP[][];
};

struct TransmitPacket{
    int sequenceNo;
    int mode;
    int command;
    int opt1;
    int opt2;
    int opt3;
};

struct ResponcePacket{
    int sequenceNo;
    int operationStatus;
    int command;
};

struct HardwareProperties{
    HardwareProperties():isDeviceConnected(false),isPortOpened(false) {}
    bool isDeviceConnected;
    bool isPortOpened;
};

#endif // STRUCTURES_H
