#ifndef WSIPROCESSINGTHREAD_H
#define WSIPROCESSINGTHREAD_H

#include <QThread>
#include <math.h>
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include<opencv/cxcore.h>

class WSIProcessingThread : public QThread
{
    Q_OBJECT
public:
    explicit WSIProcessingThread(QObject *parent = 0);

private:
    int currentResolution;
    int previousResolution;

protected:
    void run();

signals:

public slots:
    void scaleImage(int currentResolution,int previousResolution);
};

#endif // WSIPROCESSINGTHREAD_H
