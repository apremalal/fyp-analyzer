#ifndef SETTINGS_H
#define SETTINGS_H

#include "Structures.h"

class SharedSettings
{
public:
    SharedSettings();
    SerialPortSettings serialPortSettings;
    QString rawImageStoreLoc;
    QString wSIImageStoreLoc;
    QString videoStoreLoc;

    double focusThreshold;

    int zoomLevel;
    int scanningBlock;
    int cameraviewHeight;
    int cameraviewWidth;

    int currentX;
    int currentY;

    int currentXDirection;
    int currentYDirection;

    int WSImapWidth;
    int WSImapHeight;
    int overlapSize;

    int nXTiles;
    int nYTiles;

    int fullFrameStepSizeX;//steps needed to travel from one tile to another
    int fullFrameStepSizeY;
    int calculatedStepSizeX;
    int calculatedStepSizeY;

    QString frameSize;
};

#endif // SETTINGS_H
