#include "CaptureThread.h"

CaptureThread::CaptureThread(SharedImageBuffer *sharedImageBuffer, SharedSettings *sharedSettings, int deviceNumber, bool dropFrameIfBufferFull, int width, int height) : QThread(), sharedImageBuffer(sharedImageBuffer),sharedSettings(sharedSettings)
{
    // Save passed parameters
    this->dropFrameIfBufferFull=dropFrameIfBufferFull;
    this->deviceNumber=deviceNumber;
    this->width = width;
    this->height = height;
    // Initialize variables(s)
    doStop=false; doRecording = false;
    sampleNumber=0;
    fpsSum=0;
    fps.clear();
    statsData.averageFPS=0;
    statsData.nFramesProcessed=0;
}

void CaptureThread::run()
{
    while(1)
    {
        ////////////////////////////////
        // Stop thread if doStop=TRUE //
        ////////////////////////////////
        doStopMutex.lock();
        if(doStop)
        {
            doStop=false;
            doStopMutex.unlock();
            break;
        }
        doStopMutex.unlock();
        /////////////////////////////////

        // Save capture time
        captureTime=t.elapsed();
        // Start timer (used to calculate capture rate)
        t.start();

        // Synchronize with other streams (if enabled for this stream)
        sharedImageBuffer->sync(deviceNumber);

        // Capture frame (if available)
        if (!cap.grab())
            continue;

        cap.retrieve(grabbedFrame); // Retrieve frame

        // Add frame to buffer
        sharedImageBuffer->getByDeviceNumber(deviceNumber)->add(grabbedFrame, dropFrameIfBufferFull);

        if(doRecording && videoWriter->isOpened())
            videoWriter->write(grabbedFrame);

        // Update statistics
        updateFPS(captureTime);
        statsData.nFramesProcessed++;
        // Inform GUI of updated statistics
        emit updateStatisticsInGUI(statsData);
    }
    qDebug() << "Stopping capture thread...";
}

bool CaptureThread::connectToCamera()
{
    // Open camera
    bool camOpenResult = cap.open(deviceNumber);
    // Set resolution
    if(width != -1)
        cap.set(CV_CAP_PROP_FRAME_WIDTH, width);
    if(height != -1)
        cap.set(CV_CAP_PROP_FRAME_HEIGHT, height);
    // Return result
    return camOpenResult;
}

bool CaptureThread::disconnectCamera()
{
    // Camera is connected
    if(cap.isOpened())
    {
        // Disconnect camera
        cap.release();
        return true;
    }
    // Camera is NOT connected
    else
        return false;
}

void CaptureThread::updateFPS(int timeElapsed)
{
    // Add instantaneous FPS value to queue
    if(timeElapsed>0)
    {
        fps.enqueue((int)1000/timeElapsed);
        // Increment sample number
        sampleNumber++;
    }
    // Maximum size of queue is DEFAULT_CAPTURE_FPS_STAT_QUEUE_LENGTH
    if(fps.size()>CAPTURE_FPS_STAT_QUEUE_LENGTH)
        fps.dequeue();
    // Update FPS value every DEFAULT_CAPTURE_FPS_STAT_QUEUE_LENGTH samples
    if((fps.size()==CAPTURE_FPS_STAT_QUEUE_LENGTH)&&(sampleNumber==CAPTURE_FPS_STAT_QUEUE_LENGTH))
    {
        // Empty queue and store sum
        while(!fps.empty())
            fpsSum+=fps.dequeue();
        // Calculate average FPS
        statsData.averageFPS=fpsSum/CAPTURE_FPS_STAT_QUEUE_LENGTH;
        // Reset sum
        fpsSum=0;
        // Reset sample number
        sampleNumber=0;
    }
}

void CaptureThread::startRecording(){

    QDateTime local(QDateTime::currentDateTime());
    QString filePath = sharedSettings->videoStoreLoc;
    if (!QDir(filePath).exists()){
        QDir().mkpath(filePath);
    }
    int ex = static_cast<int>(cap.get(CV_CAP_PROP_FOURCC));
    videoWriter = new VideoWriter((filePath+"\\"+QString::number(local.toTime_t())+".avi").toLocal8Bit().constData(),CV_FOURCC('M','S','V','C'), statsData.averageFPS, cvSize(getInputSourceWidth(),getInputSourceHeight()),true);
    if(videoWriter->isOpened())
        doRecording = true;
}

void CaptureThread::stopRecording()
{
    doRecording = false;
    videoWriter->release();
}

void CaptureThread::captureFrame()
{
    QDateTime local(QDateTime::currentDateTime());
    QString filePath = sharedSettings->rawImageStoreLoc;
    if (!QDir(filePath).exists()){
        QDir().mkpath(filePath);
    }
    imwrite((filePath+"\\"+QString::number(local.toTime_t())+".jpg").toLocal8Bit().constData(),grabbedFrame);

}

void CaptureThread::stop()
{
    QMutexLocker locker(&doStopMutex);
    doStop=true;
}

bool CaptureThread::isCameraConnected()
{
    return cap.isOpened();
}

int CaptureThread::getInputSourceWidth()
{
    return cap.get(CV_CAP_PROP_FRAME_WIDTH);
}

int CaptureThread::getInputSourceHeight()
{
    return cap.get(CV_CAP_PROP_FRAME_HEIGHT);
}
