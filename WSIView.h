#ifndef WSIVIEW_H
#define WSIVIEW_H

#include <QWidget>
#include <QFileDialog>
#include <QWebFrame>
#include <QWebElement>
#include <QDebug>
#include <QMovie>

#include "Config.h"
#include "DeepZoomComposer.h"
#include "SharedSettings.h"

namespace Ui {
class WSIView;
}

class WSIView : public QWidget
{
    Q_OBJECT

public:
    explicit WSIView(QWidget *parent, SharedSettings *sharedSettings);
    ~WSIView();

private slots:
    void on_pushButtonLoadWSI_clicked();
    void on_pushButtonImportRawImage_clicked();
    void renderDeepZoomImage();
    void showBusy(bool show);

private:
    Ui::WSIView *ui;
    QWebFrame *frame;
    QMovie *movie;
    DeepZoomComposer *composer;
    SharedSettings *sharedSettings;
    QString importedFileName;
    void createWSI(int method);
};

#endif // WSIVIEW_H
