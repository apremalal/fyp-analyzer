#ifndef WSIFRAMELABLE_H
#define WSIFRAMELABLE_H

#include <QLabel>

class WSIFrameLable : public QLabel
{
    Q_OBJECT
public:
    WSIFrameLable(QWidget *parent = 0);

private:
    double currentResolution;
    double previousResolution;

signals:
     void onKeyPressEvent(double currentResolution);

public slots:
    void keyPressEvent(QKeyEvent *ev);
};

#endif // WSIFRAMELABLE_H
