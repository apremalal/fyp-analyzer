#ifndef PROCESSINGTHREAD_H
#define PROCESSINGTHREAD_H

#include <QtCore/QThread>
#include <QtCore/QTime>
#include <QtCore/QQueue>
#include <QDebug>
#include <QDir>
#include <QFile>
#include <QTextStream>

#include <opencv/cv.h>
#include <opencv/highgui.h>


#include "Structures.h"
#include "SharedSettings.h"
#include "Config.h"
#include "Buffer.h"
#include "MatToQImage.h"
#include "SharedImageBuffer.h"
#include "FocusMeasure.h"

using namespace cv;

class ProcessingThread : public QThread
{
    Q_OBJECT

    public:
        ProcessingThread(SharedImageBuffer *sharedImageBuffer, int deviceNumber, SharedSettings *sharedSettings, QSerialPort *serialPort);
        QRect getCurrentROI();
        void writeData(const QByteArray &data);
        void stop();
        QString initNewImageStore();
        bool writeWSIPropertiesToFile();
        struct SerialCommunicationFlags serialCommunicationFlags;

    private:
        void sendCommand();
        bool isNextTileParamsExists();
        void updateFPS(int);
        void setROI();
        void resetROI();
        void handleError(QSerialPort::SerialPortError error);
        QByteArray getEncodedTransmitPacket();

        SharedImageBuffer *sharedImageBuffer;
        SharedSettings *sharedSettings;
        QFile *WSIPropertiesFile;
        Mat currentFrame;
        Mat currentFrameGrayscale;
        Rect currentROI;
        QImage frame;
        QTime t;
        QQueue<int> fps;
        QMutex doStopMutex;
        QMutex processingMutex;
        QString currentImageStoreLoc;
        QString timeStamp;
        Size frameSize;
        Point framePoint;
        struct ImageProcessingFlags imgProcFlags;
        struct ImageProcessingSettings imgProcSettings;
        struct ThreadStatisticsData statsData;
        struct FrameData frameData;

        //serial related

        struct HardwareProperties hardwareProperties;
        struct TransmitPacket transmitPacket;
        struct ResponcePacket responcePacket;
        QSerialPort *serialPort;
        QByteArray receivedPacket;
        QByteArray buffer;

        int capturedImageCount;
        bool wSICaptured;

        volatile bool doStop;
        int processingTime;
        int fpsSum;
        int sampleNumber;
        int deviceNumber;
        bool enableFrameProcessing;

    protected:
        void run();

    private slots:
        QByteArray readData();
        void updateSerialCommunicatoinFlags(struct SerialCommunicationFlags);
        void updateImageProcessingFlags(struct ImageProcessingFlags);
        void updateImageProcessingSettings(struct ImageProcessingSettings);
        void setROI(QRect roi);

    signals:
        void scanCompleteSignal(QString);
        void newFrameData(struct FrameData);
        void newFrame(const QImage &frame);
        void updateStatisticsInGUI(struct ThreadStatisticsData);
};

#endif // PROCESSINGTHREAD_H
