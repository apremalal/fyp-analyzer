#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>
#include <QWebView>
#include <QUrl>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    sharedImageBuffer = new SharedImageBuffer();
    cameraNumber = 0; nPluginViews =0;


    QPushButton *newPluginButton = new QPushButton("+", this);
    ui->tabWidget->setCornerWidget(newPluginButton,Qt::TopRightCorner);
    sharedSettings = new SharedSettings();

    settingsView = new SettingsView(ui->tabWidget,sharedSettings);
    slideViewMap[0] = new SlideView(ui->tabWidget,cameraNumber, sharedImageBuffer, sharedSettings);
    wSIView = new WSIView(ui->tabWidget, sharedSettings);
    pluginViewMap[0] = new PluginsView(ui->tabWidget);

    ui->cameraLayout->addWidget(slideViewMap[0]);
    ui->wsiLayout->addWidget(wSIView);
    ui->pluginsLayout->addWidget(pluginViewMap[0]);
    ui->settingsLayout->addWidget(settingsView);
    ui->tabWidget->setCurrentIndex(0);
    ui->tabWidget->setTabsClosable(true);

    connect(newPluginButton, SIGNAL(clicked()),this, SLOT(on_pushButton_newPlugin_Clicked()));
    connect(ui->tabWidget,SIGNAL(tabCloseRequested(int)),this, SLOT(closePlugin(int)));
}

void MainWindow::on_pushButton_newPlugin_Clicked(){
    pluginViewMap[++nPluginViews] = new PluginsView(ui->tabWidget);
    ui->tabWidget->insertTab(2+nPluginViews,pluginViewMap[nPluginViews],QIcon(QPixmap(":/new/controller/Resources/video-on.png")),"Plugin ["+ QString::number(nPluginViews+1)+"]");
    deviceNumberMap[nPluginViews] = nPluginViews+2;
}

void MainWindow::closePlugin(int index)
{
    if(index-2>=0 && index+1!= ui->tabWidget->count() ){
        ui->tabWidget->removeTab(index);
        delete pluginViewMap[deviceNumberMap.key(index)];
        pluginViewMap.remove(deviceNumberMap.key(index));

        // Remove from map
        removeFromMapByTabIndex(deviceNumberMap, index-2);

        updateMapValues(deviceNumberMap, index-2);
        nPluginViews--;
    }
}

bool MainWindow::removeFromMapByTabIndex(QMap<int, int> &map, int tabIndex)
{
    QMutableMapIterator<int, int> i(map);
    while (i.hasNext())
    {
        i.next();
        if(i.value()==tabIndex)
        {
            i.remove();
            return true;
        }
    }
    return false;
}

void MainWindow::updateMapValues(QMap<int, int> &map, int tabIndex)
{
    QMutableMapIterator<int, int> i(map);
    while (i.hasNext())
    {
        i.next();
        if(i.value()>tabIndex)
            i.setValue(i.value()-1);
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::handleError(QSerialPort::SerialPortError error)
{;}

void MainWindow::about()
{
    QMessageBox::about(this, tr("About Simple Terminal"), tr("About"));
}

