#include "mainwindow.h"
#include <QApplication>
#include <QSplashScreen>

class InitThread : public QThread
{
protected:
    void run(void)
    {
      QThread::msleep(10); //Sleep 5 sec to simulate work ;-)
    }
};

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    app.setStyle("cleanlooks");
    QPixmap pixmap(":/new/controller/Resources/splash-screen.jpg"); //Insert your splash page image here
    if(pixmap.isNull())
    {
        QMessageBox::warning(0, "Error", "Failed to load Splash Screen image!");
    }

    QSplashScreen splash(pixmap, Qt::WindowStaysOnTopHint);
    splash.setEnabled(false); //Prevent user from closing the splash
    splash.show();
    app.processEvents(); //Make sure splash screen gets drawn ASAP

    QEventLoop loop;

    InitThread *thread = new InitThread();
    QObject::connect(thread, SIGNAL(finished()), &loop, SLOT(quit()));
    QObject::connect(thread, SIGNAL(terminated()), &loop, SLOT(quit()));
    thread->start();

    loop.exec(); //Do event processing until the thread has finished!
    splash.hide();

    MainWindow mainWin;
    mainWin.setWindowTitle("Analyzer");
    mainWin.showMaximized();
    return app.exec();
}

