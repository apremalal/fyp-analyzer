#ifndef PLUGINSVIEW_H
#define PLUGINSVIEW_H

#include <QWidget>
#include <QDebug>
#include <QMessageBox>
#include <QFileDialog>
#include <QDir>
#include <QFile>

#include <json/writer.h>

#include "MatToQImage.h"
#include "engine.h"
#include <QMovie>

#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <opencv/cxcore.h>

#include "MicrofilariaePlugin.h"
namespace Ui {
    class PluginsView;
}
using namespace cv;

class PluginsView : public QWidget
{
    Q_OBJECT

public:
    explicit PluginsView(QWidget *parent = 0);
    ~PluginsView();

private:
    Ui::PluginsView *ui;
    Mat processedImg,originalImgTemp,originalImg,dst;
    double currentResolution;

    Engine *matlabEngine;
    mxArray *result;
    QMovie *movie;
    QString loadedFileName;

    MicrofilariaePlugin *microfilariaePlugin;
    void renderImage(Mat img);
    void showBusy(bool show);
    int segmentImageToSlidingWindows(int windowWidth,int windowHeight,int overlapPrecentage,Mat img,QString dstDirectory);
    int* mapToGlobalCordinates(int x, int y,int wX,int wY,int windowWidth,int windowheight,int overlap);


public slots:
    void scaleImage(double currentResolution);
    void updateFrame(const QImage &frame);
    void on_pushButtonZoom_pressed();

private slots:
    void on_pushButtonLoadRecentWSI_clicked();
    void on_pushButtonAnalyze_clicked();
    void on_pushButtonOpenWSI_clicked();
    void on_pushButtonReset_clicked();
    void process_result(Mat img);
};

#endif // PLUGINSVIEW_H
