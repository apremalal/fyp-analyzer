#include "FocusMeasure.h"
#include <QDebug>

double FocusMeasure(Mat image)
{
    Mat gray_image;
    cvtColor( image, gray_image, CV_BGR2GRAY );    // GS Conversion

    Mat Lap_K = (Mat_<double>(3,3) << 0.1667, 0.6667, 0.1667, 0.6667, -3.3333, 0.6667, 0.1667, 0.6667, 0.1667); // Laplace Kernel
    Mat laplace_image;
    Point anchor = Point(0,0);
    float delta = 0.0;
    filter2D(gray_image,laplace_image,-1,Lap_K,anchor,delta,BORDER_REPLICATE);  // 2D convolution

    Scalar mean;
    Scalar stddev;

    meanStdDev(laplace_image,mean,stddev);              // Standard Deviation of the Matrix

    double focus_Measure = stddev.val[0]*stddev.val[0];        // Output (Variance of the Laplacian)
    return focus_Measure;
}
