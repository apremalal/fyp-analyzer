QT += webkit webkitwidgets
QT += serialport
QT += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Analyzer
TEMPLATE = app

INCLUDEPATH += "C:\Program Files\MATLAB\R2013a\extern\include" \
 H:\Setup\opencv\build\include \
 C:\Users\Anuruddha\git\FYP\fyp-analyzer\dependencies\include \



CONFIG(release,release)
{
    LIBS += H:\Setup\opencv\build\x64\vc11\lib\opencv_core248.lib \
    H:\Setup\opencv\build\x64\vc11\lib\opencv_highgui248.lib \
    H:\Setup\opencv\build\x64\vc11\lib\opencv_imgproc248.lib \
    "C:\Program Files\MATLAB\R2013a\extern\lib\win64\microsoft\libeng.lib" \
    "C:\Program Files\MATLAB\R2013a\extern\lib\win64\microsoft\libmx.lib" \
    "C:\Program Files\MATLAB\R2013a\extern\lib\win64\microsoft\libut.lib" \
    "C:\Program Files\MATLAB\R2013a\extern\lib\win64\microsoft\libmat.lib" \
}


SOURCES += main.cpp\
    SharedImageBuffer.cpp \
    CaptureThread.cpp \
    FrameLabel.cpp \
    ProcessingThread.cpp \
    MatToQImage.cpp \
    SlideView.cpp \
    FocusMeasure.cpp \
    SettingsView.cpp \
    PluginsView.cpp \
    WSIView.cpp \
    WSICreator.cpp \
    MicrofilariaePlugin.cpp \
    DeepZoomComposer.cpp \
    WSIProcessingThread.cpp \
    SharedSettings.cpp \
    Mainwindow.cpp \
    WSIFramelable.cpp \
    json_reader.cpp \
    json_value.cpp \
    json_writer.cpp

HEADERS  += \
    SharedImageBuffer.h \
    Buffer.h \
    CaptureThread.h \
    Structures.h \
    Config.h \
    FrameLabel.h \
    ProcessingThread.h \
    MatToQImage.h \
    SlideView.h \
    FocusMeasure.h \
    SettingsView.h \
    PluginsView.h \
    WSIView.h \
    WSICreator.h \
    MicrofilariaePlugin.h \
    DeepZoomComposer.h \
    WSIProcessingThread.h \
    SharedSettings.h \
    Mainwindow.h \
    WSIFramelable.h \
    json_batchallocator.h

FORMS    += mainwindow.ui \
    SlideView.ui \
    SettingsView.ui \
    PluginsView.ui \
    WSIView.ui

RESOURCES += \
    Analyzer_resources.qrc \
