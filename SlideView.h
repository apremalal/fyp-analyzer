#ifndef CAMERAVIEW_H
#define CAMERAVIEW_H

#include "CaptureThread.h"
#include "ProcessingThread.h"
#include "Structures.h"
#include "SharedImageBuffer.h"
#include "Config.h"
#include "SharedSettings.h"

#include <QAction>
#include <QSerialPort>
#include <QSerialPortInfo>

namespace Ui {
class SlideView;
}

class SlideView : public QWidget
{
    Q_OBJECT

public:
    explicit SlideView(QWidget *parent, int deviceNumber, SharedImageBuffer *sharedImageBuffer, SharedSettings *sharedSettings);
    ~SlideView();
    bool openSerialPort();
    bool connectToCamera(bool dropFrame, int capThreadPrio, int procThreadPrio, bool createProcThread, int width, int height);
    bool connectToDevice();
    bool closeDeviceConnection();
    bool isPortOpened();
    bool isDeviceConnected();

private:
    Ui::SlideView *ui;
    SharedSettings *sharedSettings;
    QSerialPort *serialPort;
    ProcessingThread *processingThread;
    CaptureThread *captureThread;
    SharedImageBuffer *sharedImageBuffer;
    SerialPortSettings serialPortSettings;
    HardwareProperties hardwareProperties;
    ImageProcessingFlags imageProcessingFlags;
    SerialCommunicationFlags serialCommunicationFlags;
    void initController();
    void stopCaptureThread();
    void stopProcessingThread();
    void setManualControlEnabled(bool enable);
    int deviceNumber;
    bool isCameraConnected;
    bool doRecord;
    QString totalPacket;
    QByteArray buffer;

public slots:
    void setImageProcessingSettings();
    void newMouseData(struct MouseData mouseData);
    void updateMouseCursorPosLabel();
    void clearImageBuffer();
    QByteArray readData();

private slots:
    void connectDisconnectCamera();
    void on_navigation_pushButton_pressed();
    void on_navigation_pushButton_released();
    void on_pushButtonStartStopScanning_clicked();
    void on_zoomLevel_changed();
    void finalizeScan(QString WSILocation);
    void updateFrame(const QImage &frame);
    void updateFrameData(struct FrameData frameData);
    void updateProcessingThreadStats(struct ThreadStatisticsData statData);
    void updateCaptureThreadStats(struct ThreadStatisticsData statData);
    void handleContextMenuAction(QAction *action);
    void on_cameraViewSizeBox_currentIndexChanged(int index);
    void on_scanningBlockBox_currentIndexChanged(int index);

    void on_pushButtonUpdateFocus_clicked();
    void on_pushButtonRecord_clicked();
    void on_pushButtonCaptureTile_clicked();

    void on_pushButtonDeviceConnectDisconnect_clicked();

signals:
    void newSerialCommunicationFlags(struct SerialCommunicationFlags serialCommunicationFlags);
    void newImageProcessingFlags(struct ImageProcessingFlags imageProcessingFlags);
    void setROI(QRect roi);
};

#endif // CAMERAVIEW_H
