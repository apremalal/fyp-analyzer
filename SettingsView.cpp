#include "SettingsView.h"
#include "ui_SettingsView.h"
#include <QtSerialPort/QSerialPort>
#include <QFileDialog>
#include <QSerialPort>
#include <QDebug>

SettingsView::SettingsView(QWidget *parent, SharedSettings *sharedSettings) :
    QWidget(parent),
    ui(new Ui::SettingsView),sharedSettings(sharedSettings)
{
    ui->setupUi(this);
    intValidator = new QIntValidator(0, 4000000, this);

    connect(ui->applyButton, SIGNAL(clicked()), this, SLOT(updateSettings()));
    connect(ui->baudRateBox, SIGNAL(currentIndexChanged(int)),this, SLOT(checkCustomBaudRatePolicy(int)));
    initUiDefaultParams();
}

SettingsView::~SettingsView()
{
    delete ui;
}
void SettingsView::initUiDefaultParams(){

    /***** Serial posrt data ****/

    ui->baudRateBox->setInsertPolicy(QComboBox::NoInsert);

    ui->baudRateBox->addItem(QLatin1String("9600"), QSerialPort::Baud9600);
    ui->baudRateBox->addItem(QLatin1String("19200"), QSerialPort::Baud19200);
    ui->baudRateBox->addItem(QLatin1String("38400"), QSerialPort::Baud38400);
    ui->baudRateBox->addItem(QLatin1String("115200"), QSerialPort::Baud115200);
    ui->baudRateBox->addItem(QLatin1String("Custom"));

    sharedSettings->serialPortSettings.baudRate = QSerialPort::Baud9600;
    // fill data bits
    ui->dataBitsBox->addItem(QLatin1String("5"), QSerialPort::Data5);
    ui->dataBitsBox->addItem(QLatin1String("6"), QSerialPort::Data6);
    ui->dataBitsBox->addItem(QLatin1String("7"), QSerialPort::Data7);
    ui->dataBitsBox->addItem(QLatin1String("8"), QSerialPort::Data8);
    ui->dataBitsBox->setCurrentIndex(3);

    sharedSettings->serialPortSettings.dataBits = QSerialPort::Data8;

    // fill parity
    ui->parityBox->addItem(QLatin1String("None"), QSerialPort::NoParity);
    ui->parityBox->addItem(QLatin1String("Even"), QSerialPort::EvenParity);
    ui->parityBox->addItem(QLatin1String("Odd"), QSerialPort::OddParity);
    ui->parityBox->addItem(QLatin1String("Mark"), QSerialPort::MarkParity);
    ui->parityBox->addItem(QLatin1String("Space"), QSerialPort::SpaceParity);

    sharedSettings->serialPortSettings.parity = QSerialPort::NoParity;

    // fill stop bits
    ui->stopBitsBox->addItem(QLatin1String("1"), QSerialPort::OneStop);
#ifdef Q_OS_WIN
    ui->stopBitsBox->addItem(QLatin1String("1.5"), QSerialPort::OneAndHalfStop);
#endif
    ui->stopBitsBox->addItem(QLatin1String("2"), QSerialPort::TwoStop);

    sharedSettings->serialPortSettings.stopBits = QSerialPort::OneStop;

    // fill flow control
    ui->flowControlBox->addItem(QLatin1String("None"), QSerialPort::NoFlowControl);
    ui->flowControlBox->addItem(QLatin1String("RTS/CTS"), QSerialPort::HardwareControl);
    ui->flowControlBox->addItem(QLatin1String("XON/XOFF"), QSerialPort::SoftwareControl);

    sharedSettings->serialPortSettings.flowControl = QSerialPort::NoFlowControl;

#ifdef _WIN32
    ui->lineEditRawImageStoreLoc->setText(RAW_IMAGE_STORE);
    ui->lineEditWSIStoreLoc->setText(WSI_IMAGE_STORE);
    ui->lineEditVideoStoreLoc->setText(DEFAULT_VIDEO_STORE);
    sharedSettings->rawImageStoreLoc = RAW_IMAGE_STORE;
    sharedSettings->wSIImageStoreLoc = WSI_IMAGE_STORE;
    sharedSettings->videoStoreLoc = DEFAULT_VIDEO_STORE;
#else
    ;
#endif
}

void SettingsView::checkCustomBaudRatePolicy(int idx)
{
    bool isCustomBaudRate = !ui->baudRateBox->itemData(idx).isValid();
    ui->baudRateBox->setEditable(isCustomBaudRate);
    if (isCustomBaudRate) {
        ui->baudRateBox->clearEditText();
        QLineEdit *edit = ui->baudRateBox->lineEdit();
        edit->setValidator(intValidator);
    }
}

void SettingsView::updateSettings()
{
    // Baud Rate
    if (ui->baudRateBox->currentIndex() == 4) {
        // custom baud rate
        sharedSettings->serialPortSettings.baudRate = ui->baudRateBox->currentText().toInt();
    } else {
        // standard baud rate
        sharedSettings->serialPortSettings.baudRate = static_cast<QSerialPort::BaudRate>(
                    ui->baudRateBox->itemData(ui->baudRateBox->currentIndex()).toInt());
    }

    // Data bits
    sharedSettings->serialPortSettings.dataBits = static_cast<QSerialPort::DataBits>(
                ui->dataBitsBox->itemData(ui->dataBitsBox->currentIndex()).toInt());
    sharedSettings->serialPortSettings.stringDataBits = ui->dataBitsBox->currentText();

    // Parity
    sharedSettings->serialPortSettings.parity = static_cast<QSerialPort::Parity>(
                ui->parityBox->itemData(ui->parityBox->currentIndex()).toInt());
    sharedSettings->serialPortSettings.stringParity = ui->parityBox->currentText();

    // Stop bits
    sharedSettings->serialPortSettings.stopBits = static_cast<QSerialPort::StopBits>(
                ui->stopBitsBox->itemData(ui->stopBitsBox->currentIndex()).toInt());
    sharedSettings->serialPortSettings.stringStopBits = ui->stopBitsBox->currentText();

    // Flow control
    sharedSettings->serialPortSettings.flowControl = static_cast<QSerialPort::FlowControl>(
                ui->flowControlBox->itemData(ui->flowControlBox->currentIndex()).toInt());
    sharedSettings->serialPortSettings.stringFlowControl = ui->flowControlBox->currentText();

    // Additional options
    sharedSettings->serialPortSettings.localEchoEnabled = ui->localEchoCheckBox->isChecked();
}

void SettingsView::on_pushButtonSelectImageStore_clicked()
{
    QFileDialog fd;
    QString rawImageStoreLoc = fd.getExistingDirectory(this, tr("Select Folder"),"");
    ui->lineEditRawImageStoreLoc->setText(rawImageStoreLoc);
    sharedSettings->rawImageStoreLoc = rawImageStoreLoc;
}

void SettingsView::on_pushButtonSelectWSIStore_clicked()
{
    QFileDialog fd;
    QString wSiStoreLoc = fd.getExistingDirectory(this, tr("Select Folder"),"");
    ui->lineEditWSIStoreLoc->setText(wSiStoreLoc);
    sharedSettings->wSIImageStoreLoc = wSiStoreLoc;
}
