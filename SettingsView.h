#ifndef SETTINGSVIEW_H
#define SETTINGSVIEW_H

#include "Structures.h"
#include "SharedSettings.h"
#include "Config.h"
#include <QIntValidator>
#include <QWidget>

namespace Ui {
    class SettingsView;
}

class SettingsView : public QWidget
{
    Q_OBJECT

public:
    explicit SettingsView(QWidget *parent, SharedSettings *sharedSettings);
    ~SettingsView();

private:
    Ui::SettingsView *ui;
    void initUiDefaultParams();
    SharedSettings *sharedSettings;
    QIntValidator *intValidator;

private slots:
    void updateSettings();
    void checkCustomBaudRatePolicy(int idx);
    void on_pushButtonSelectImageStore_clicked();
    void on_pushButtonSelectWSIStore_clicked();

signals:
    void newSerialPortSettings(struct SerialPortSettings serialPortSettings);
};

#endif // SETTINGSVIEW_H
