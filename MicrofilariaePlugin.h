#ifndef MICROFILARIAEPLUGIN_H
#define MICROFILARIAEPLUGIN_H

#include <QtCore/QThread>

#include "MatToQImage.h"
#include "engine.h"

#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <opencv/cxcore.h>

class MicrofilariaePlugin : public QThread
{
    Q_OBJECT
public:
    MicrofilariaePlugin(Mat *srcImage, QString imageFileName);

private:
    Mat *srcImage;
    Engine *matlabEngine;
    mxArray *result;
    QString imageFileName;

protected:
    void run();

signals:
    void matLabResults(Mat result);
};

#endif // MICROFILARIAEPLUGIN_H
