#include "PluginsView.h"
#include "ui_PluginsView.h"
#include <QDebug>

PluginsView::PluginsView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PluginsView)
{
    ui->setupUi(this);
    ui->comboBoxPlugin->addItem(QLatin1String("Microfilariae Plugin"), 1);
    ui->comboBoxPlugin->addItem(QLatin1String("RBC count"), 2);
    currentResolution = 100;
    movie = new QMovie(":/new/controller/Resources/progressBar.gif");
    ui->labelProgress->setVisible(false);
    ui->labelProgress->setMovie(movie);
    movie->start();
    connect(ui->frameWSILabel, SIGNAL(onKeyPressEvent(double)), this, SLOT(scaleImage(double)));
    connect(ui->pushButtonZoomIn, SIGNAL(clicked()),this, SLOT(on_pushButtonZoom_pressed()));
    connect(ui->pushButtonZoomOut, SIGNAL(clicked()),this, SLOT(on_pushButtonZoom_pressed()));
    qRegisterMetaType<Mat>("Mat");
}

PluginsView::~PluginsView()
{
    delete ui;
}

void PluginsView::updateFrame(const QImage &frame)
{
    ui->frameWSILabel->setPixmap(QPixmap::fromImage(frame));
}

void PluginsView::on_pushButtonLoadRecentWSI_clicked()
{
    showBusy(true);
    originalImgTemp = imread("H:/Academic/ENTC/FYP/Code/test.jpg");
    loadedFileName = "H:/Academic/ENTC/FYP/Code/test.jpg";
    originalImg = originalImgTemp.clone();

    if(!originalImgTemp.data)
        qDebug() << "Unable to load the WSI";
    processedImg = originalImgTemp.clone();

    QImage frame=MatToQImage(originalImgTemp);
    ui->frameWSILabel->setPixmap(QPixmap::fromImage(frame));
    ui->frameWSILabel->setMinimumWidth(frame.width());
    ui->frameWSILabel->setMinimumHeight(frame.height());
    showBusy(false);
}

void PluginsView::on_pushButtonZoom_pressed()
{
    if(processedImg.data)
    {
        QPushButton *button = (QPushButton *)sender();
        QString objectName = button->objectName();
        double newresolution = 100;
        if(objectName.compare("pushButtonZoomIn") == 0)
        {
            newresolution = (currentResolution==100 || (currentResolution+10) >= 100)?100: (currentResolution+10);
        }else{
            newresolution = (currentResolution==10 || (currentResolution-10) <= 0)? 10: (currentResolution-10);
        }

        if (newresolution != currentResolution)
        {
            scaleImage(newresolution);
        }
    }
}

void PluginsView::scaleImage(double currentResolution){
    this->currentResolution = currentResolution;
    processedImg = originalImgTemp.clone();
    double scale = currentResolution/100;
    int clns = processedImg.cols*scale;
    int rws = processedImg.rows*scale;

    cv::resize(originalImgTemp,processedImg,Size(clns,rws),0,0,INTER_LINEAR);
    renderImage(processedImg);
}

void PluginsView::on_pushButtonAnalyze_clicked()
{
    if(originalImgTemp.data)
    {
        showBusy(true);
        segmentImageToSlidingWindows(640,480,20,originalImgTemp,"");
        microfilariaePlugin = new MicrofilariaePlugin(&originalImgTemp,loadedFileName);
        microfilariaePlugin->start((QThread::Priority)5);
        connect(microfilariaePlugin, SIGNAL(matLabResults(Mat)), this, SLOT(process_result(Mat)));
    }
}

int PluginsView::segmentImageToSlidingWindows(int windowWidth,int windowHeight,int overlapPrecentage,Mat img,QString dstDirectory)
{
    int imgHeight = img.rows;
    int imgWidth = img.cols;

    if(imgHeight < windowHeight || imgWidth < windowWidth)
        return false;

    Json::Value result;

    int overlapSize = overlapPrecentage * windowWidth/100;
    int singleStepLengthX = windowWidth - overlapSize;
    int singleStepLengthY = windowHeight - overlapSize;

    int nX = imgWidth/singleStepLengthX;
    int nY = imgHeight/singleStepLengthY;

    int currentX = 0;
    int currentY = 0;
    int windowCount;

    for(int i=0;i<nY;i++)
    {
        for(int j=0;j<nX;j++)
        {
            if(currentX+windowWidth <= imgWidth && currentY+windowHeight <= imgHeight){
                Mat to(img,Range(currentY, currentY + windowHeight)
                       ,Range(currentX, currentX + windowWidth));
                QString resultImg = dstDirectory+QString::number(i)+","+QString::number(j)+".jpg";

                imwrite(resultImg.toLocal8Bit().constData(),to);
                currentX += singleStepLengthX;
                windowCount++;
            }
        }
        currentX = 0;
        currentY += singleStepLengthY;
    }

    QFile resultJson(dstDirectory+"config.json");
    resultJson.open(QIODevice::WriteOnly|QIODevice::Text);
    QTextStream out(&resultJson);
    result["totalImages"] = windowCount ;
    result["overlapSize"] = overlapPrecentage;
    result["windowWidth"] = windowWidth;
    result["windowHeight"] = windowHeight;
    result["originalImgHeight"] = imgHeight;
    result["originalImgWidth"] = imgWidth;

    out << QString::fromStdString(result.toStyledString());
    out.flush();
    return windowCount;
}

int* PluginsView::mapToGlobalCordinates(int x, int y,int wX,int wY,int windowWidth,int windowheight,int overlap){
    int globalCordinates[2] = {-1,-1};

    globalCordinates[0] = x*wX*(1-overlap/100);
    globalCordinates[1] = y*wY*(1-overlap/100);

    return globalCordinates;
}

void PluginsView::on_pushButtonOpenWSI_clicked()
{
    QFileDialog fd;
    QString selfilter = tr("JPEG (*.jpg *.jpeg)");
    loadedFileName = fd.getOpenFileName(this, tr("Select File"),"", tr("All files (*.*);;JPEG (*.jpg *.jpeg);;TIFF (*.tif)" ),&selfilter );
    originalImgTemp = imread(loadedFileName.toLocal8Bit().constData());
    originalImg = originalImgTemp.clone();
    if(originalImgTemp.data){
        processedImg = originalImgTemp.clone();
        renderImage(originalImgTemp);
    }
}

void PluginsView::on_pushButtonReset_clicked()
{
    if(originalImg.data)
        originalImgTemp = originalImg.clone();
    if (processedImg.data)
        processedImg = NULL;
    renderImage(originalImgTemp);
}

void PluginsView::process_result(Mat img){
    showBusy(false);
    if(img.data){
        originalImgTemp = img;
        renderImage(img);
    }
}

void PluginsView::renderImage(Mat img)
{
    QImage frame=MatToQImage(img);
    ui->frameWSILabel->setPixmap(QPixmap::fromImage(frame));
    ui->frameWSILabel->setMinimumWidth(frame.width());
    ui->frameWSILabel->setMinimumHeight(frame.height());
}

void PluginsView::showBusy(bool show)
{
    ui->labelProgress->setVisible(show);
}

