#include "WSICreator.h"

WSICreator::WSICreator()
{
}

Mat WSICreator::createStitchedImage(){
    //TO DO:
    Mat result =  Mat::zeros(4,5,CV_8UC1);
    return result;
}

Mat WSICreator::createTiledImage(vector<cv::Mat> & images, int cols, int min_gap_size){
    // let's first find out the maximum dimensions
    int max_width = 0;
    int max_height = 0;
    for ( int i = 0; i < images.size(); i++) {
        if ( i > 0 && images[i].type() != images[i-1].type() ) {
            qDebug() << "WARNING:createOne failed, different types of images";
            return Mat();
        }
        max_height = std::max(max_height, images[i].rows);
        max_width = std::max(max_width, images[i].cols);
    }
    // number of images in y direction
    int rows = ceil(double(images.size()/ cols));

    // create our result-matrix
    Mat result = Mat::zeros(rows*max_height + (rows-1)*min_gap_size,
                            cols*max_width + (cols-1)*min_gap_size, images[0].type());
    size_t i = 0;
    int current_height = 0;
    int current_width = 0;
    for ( int y = 0; y < rows; y++ ) {
        for ( int x = 0; x < cols; x++ ) {
            if ( i >= images.size() ) // shouldn't happen, but let's be safe
                return result;
            // get the ROI in our result-image
            Mat to(result,
                   Range(current_height, current_height + images[i].rows),
                   Range(current_width, current_width + images[i].cols));
            // copy the current image to the ROI
            images[i++].copyTo(to);
            current_width += max_width + min_gap_size;
        }
        // next line - reset width and update height
        current_width = 0;
        current_height += max_height + min_gap_size;
    }
    return result;
}

