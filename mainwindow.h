#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtSerialPort/QSerialPort>
#include <QIntValidator>
#include <QLineEdit>
#include <QMessageBox>
#include <QPushButton>

#include "WSIView.h"
#include "SlideView.h"
#include "SettingsView.h"
#include "Buffer.h"
#include "SharedImageBuffer.h"
#include "PluginsView.h"
#include "SharedSettings.h"
#include "Structures.h"
#include "engine.h"

namespace Ui {
    class MainWindow;
}

class QIntValidator;

class MainWindow : public QMainWindow
{
    Q_OBJECT

private:
    void updateSettings();
    bool removeFromMapByTabIndex(QMap<int, int>& map, int tabIndex);
    void updateMapValues(QMap<int, int>& map, int tabIndex);

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
     SharedSettings *sharedSettings;
private:
    Ui::MainWindow *ui;       
    SharedImageBuffer *sharedImageBuffer;
    WSIView *wSIView;
    SettingsView *settingsView;
    QMap<int, int> deviceNumberMap;
    QMap<int, SlideView*> slideViewMap;
    QMap<int,PluginsView*> pluginViewMap;
    int nPluginViews;
    int cameraNumber;

private slots:
    void about();
    void on_pushButton_newPlugin_Clicked();
    void closePlugin(int index);
    void handleError(QSerialPort::SerialPortError error);
};
#endif // MAINWINDOW_H
