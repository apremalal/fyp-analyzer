#include "DeepZoomComposer.h"

DeepZoomComposer::DeepZoomComposer(int compositionType, QString source, QString destination, int tileSize, double imageQuality, int tileOverlap,bool createCollection):QThread()
{
    this->compositionType = compositionType;
    this->source = source;
    this->destination = destination;
    this->tileSize = tileSize;
    this->tileOverlap = tileOverlap;
    this->imageQuality = imageQuality;
    this->createCollection = createCollection;
    pZoomComposer = new IDeepZoomComposerPtr(__uuidof(Composer));
}

void DeepZoomComposer::run(){
    switch (compositionType) {
    case SINGLE_IMAGE_COMPOSER:
        pZoomComposer->composeSingleImage(source.toUtf8().constData(),destination.toUtf8().constData(), tileSize, imageQuality, tileOverlap);
        emit compositionComplete();
        break;
    case COLLECTION_COMPOSER:
        pZoomComposer->composeMultipleImages(source.toUtf8().constData(),destination.toUtf8().constData(),createCollection, tileSize, imageQuality, tileOverlap);
        emit compositionComplete();
        break;
    }
}
